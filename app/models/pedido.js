/*!
 * Module dependencies
 */

// var mongoose = require('mongoose');
// var Schema = mongoose.Schema;
// var Exame = mongoose.model('Exame');

// var shortid = require('shortid');

// /**
//  * Pedido schema
//  */

// var PedidoSchema = new Schema({
//   _id: { type: String, unique: true, 'default': shortid.generate },
//   medico: { type: Schema.ObjectId, ref : 'User'},
//   exames: [{type: Schema.ObjectId, ref: 'Exame'}],
//   emailPaciente: { type: String, required: true },
// });


// PedidoSchema
//   .virtual('nomesExames')
//   .set(function(nomes){
//     var context = this;
//     Exame.find({ _id: { $in: nomes }}).exec(function(err, exames){
//       if(err){
//         return err;
//       }
//       if(nomes.length !== exames.length){
//         console.log('Nao adicionou todos exames');
//       }
//       for(var i = 0; i < exames.length; i++){
//         context.exames.push(exames[i]);
//       }
//       context.save();
//     });
//   })
//   .get(function(){
//     var names = '';
//     Exame.find({ _id: { $in: this.exames } }).select('name').exec(function(err, exames){
//       for(var i = 0; i < exames.length; i++){
//         names+= exames[i].name;
//         if(i !== exames.length){
//           names+= ',';
//         }
//       }
//       return names;
//     });
//   });

// // PedidoSchema.methods = {
// //   addExames: function(names){
// //     Exame.find({ name: { $in: names }}).exec(function(err, exames){
// //       var arr = [];
// //       if(err){
// //         return err;
// //       }
// //       if(names.length !== exames.length){
// //         console.log('Nao adicionou todos exames');
// //       }
// //       for(var i = 0; i < exames.length; i++){
// //         arr.push(exames[i]);
// //       }
// //       return arr;
// //     });
// //   }
// // };

// /**
//  * Register
//  */

// mongoose.model('Pedido', PedidoSchema);