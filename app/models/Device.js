/*!
 * Module dependencies
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Device schema
 */

var DeviceSchema = new Schema({
  user: { type : Schema.ObjectId, ref : 'User' },
  temperature: { type : Number },
  culture: {type: Schema.ObjectId, ref : 'Culture'},
  lightTimer: { type: Number },
  waterTimer: { type: Number },
  waterLast: { type: Date, default : Date.now },
  lightLast: { type: Date, default : Date.now },
  lightDuration: { type: Number },
  light: { type: Boolean, default: true },
  water: { type: Boolean, default: true },
  lightStatus: { type: Boolean, default: false}
});

/**
 * Register
 */

mongoose.model('Device', DeviceSchema);