/*!
 * Module dependencies
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Device schema
 */

var CultureSchema = new Schema({
  name: { type: String, default: '' },
  temperature: { type : Number },
  lightTimer: { type: Number },
  lightDuration: { type: Number }
});

/**
 * Register
 */

mongoose.model('Culture', CultureSchema);