var mongoose = require('mongoose');
var User = mongoose.model('User');
var Device = mongoose.model('Device');
var Culture = mongoose.model('Culture');
// var io = require('socket.io')(server);

// io.on('connection', function(socket) {
//   console.log('user conected')
//   socket.on('disconnect', function(){
//     console.log('user disconnected');
//   });
  
// });


exports.createDevice = function(req, res){
  var device = new Device(req.body);
  // device.user = req.user;
  device.save();

  console.log(device);
  return res.redirect('/device/'+device.id);
};

// exports.editPedido = function(req, res){
//   Pedido.findOne({ _id: req.body.pedidoId }).exec(function(err, pedido){
//     if(err){
//       return err;
//     }
//     //todo exames tem uma logica mais complexa
//     pedido.nomesExames = req.body.nomesExames || pedido.nomesExames;
//     pedido.emailPaciente = req.body.email  || pedido.emailPaciente;

//     pedido.save();
//     return res.render('index/home');
//   });devices
// };

exports.all =  function(req, res){

  console.log(req.user.role);

  if(req.user.role == 'admin'){
    Device.find().populate({path: 'culture', select: 'name'}).populate({path: 'user', select: 'name'}).exec(function(err, devices){
      return res.render('devices/all', {devices: devices, user: req.user, role: req.user.role});
    });
  } else {
    Device.find({user: req.user.id}).populate({ path: 'culture', select: 'name'}).exec(function(err, devices){
      
      console.log(devices);

      var send = {devices: devices, usuario: req.user, role: req.user.role};

      console.log(send.usuario);
     
      return res.render('devices/all', send);
     
    });
  }
};

exports.restDevices =  function(req, res){
  var userid = req.params.userid;
  Device.find({user: userid}).exec(function(err, devices){
    
    console.log(devices);
    if(devices.length>0){
      return res.send({success: true, devices: devices});
    } else {
      return res.send({success: false});
    }
   
  });
};

exports.restUpdate =  function(req, res){

  console.log('UPDATE');
  var deviceID = req.params.idDevice;
  var temperature = req.params.temperature;
  var lightTimer = req.params.lightTimer;
  var lightDuration = req.params.lightDuration;
  var lightStatus = req.params.lightStatus;
  var waterTimer = req.params.waterTimer;

  Device.findOne({_id: deviceID}).exec(function(err, device){
    
    device.temperature = temperature;
    device.lightTimer = lightTimer;
    device.lightDuration = lightDuration;
    device.lightStatus = lightStatus;
    device.waterTimer = waterTimer;

    device.save();

    return res.send({success: true, message: "Dispositivo alterado com sucesso"});
   
  });
};


exports.edit = function(req, res){
  var idDevice = req.params.idDevice || req.body.idDevice;
  Device.find({_id: idDevice}).populate({ path: 'culture', select: 'name'}).exec(function(err, devices){
    if(req.body.user){
      devices[0].user = req.body.user; 
    }

    var prevCulture = devices[0].culture;
    devices[0].culture = req.body.culture;

    Culture.find({_id: req.body.culture}).exec(function(err, cultures){

      if(cultures[0].name == 'Personalizado'){
        devices[0].waterTimer = req.body.waterTimer; 
        devices[0].lightTimer = req.body.lightTimer; 
        devices[0].lightDuration = req.body.lightDuration;
        devices[0].temperature = req.body.temperature;
        if(req.body.lightStatus == "on"){
          // console.log('LIGAR LUZ');
          devices[0].lightStatus = true;
        } else {
          // console.log('DESLIGAR LUZ');
          devices[0].lightStatus = false;
        }
      } else {
        devices[0].lightTimer =  cultures[0].lightTimer;
        devices[0].lightDuration = cultures[0].lightDuration;
        devices[0].temperature = cultures[0].temperature;
      }


      // socket.emit('edit', { hello: 'world' });

      devices[0].save(); 

      // console.log(devices[0]);
     
      return res.redirect('/device/'+req.body.idDevice);     
    });

  });
};

exports.viewDevice = function(req, res){

  var idDevice = req.params.idDevice || req.body.idDevice;
  Device.find({_id: idDevice}).populate({ path: 'user', select: 'name'}).exec(function(err, devices){
    if(devices.length){
     return res.render('devices/show', {device:devices});
    } else {
     return res.render('home/index', {busca: true});
    }
  });

};

exports.editView = function(req, res){

  var idDevice = req.params.idDevice || req.body.idDevice;
  Device.find({_id: idDevice}).populate({ path: 'user', select: 'name'}).exec(function(err, devices){
    if(devices.length){
      Culture.find().exec(function(err, cultures){
        return res.render('devices/edit', {device:devices[0], cultures: cultures});
      });
    } else {
     return res.render('home/index', {busca: true});
    }
  });

};

exports.ping = function (req, res) {
  var idDevice = req.params.idDevice || req.body.idDevice;
  // console.log('api dev ping: '+ idDevice);
  res.setHeader('Content-Type', 'application/json');
  Device.find({_id:  idDevice}).exec(function(err, devices){
    // console.log(idDevice);


    if(devices.length>0){
     var lightLast = new Date(devices[0].lightLast);
      var lightTimer = devices[0].lightTimer;

      var waterLast = new Date(devices[0].waterLast);
      var waterTimer = devices[0].waterTimer;
      
      var now = new Date();

      var timeDiff = Math.abs(lightLast.getTime() - now.getTime());
      var diffMins = Math.ceil(timeDiff / (1000 * 60));     

      var waterDiff = Math.abs(waterLast.getTime() - now.getTime());
      var waterMins = Math.ceil(waterDiff / (1000 * 60));     
      
      if(devices[0].light == true){

        if(diffMins > devices[0].lightDuration){
          
          if(devices[0].light == true){
            devices[0].light = false;
            devices[0].save();
          }

        } 

      } else {
        var timer = devices[0].lightDuration + devices[0].lightTimer;
        if(diffMins > timer){
          devices[0].light = true;
          devices[0].lightLast = new Date();
          devices[0].save();
        }
      }

      if(waterMins > devices[0].waterTimer){
        devices[0].water = true;
        devices[0].waterLast = new Date();
        devices[0].save();
      } else {
        if(devices[0].water == true){
          devices[0].water = false;
          devices[0].save();
        }
      }


      var intLightStatus;

      if(devices[0].lightStatus){
        intLightStatus = 1;
      } else {
        intLightStatus = 0;
      }

      console.log('devices:');
      console.log({
        temperature: devices[0].temperature,
        lightTimer: devices[0].lightTimer,
        lightDuration: devices[0].lightDuration,
        waterTimer: devices[0].waterTimer,
        lightStatus: intLightStatus
      });

      

      
      return res.send({
        temperature: devices[0].temperature,
        lightTimer: devices[0].lightTimer,
        lightDuration: devices[0].lightDuration,
        waterTimer: devices[0].waterTimer,
        lightStatus: intLightStatus
      });
    }

  });
};

exports.lightStatus = function (req, res) {
  var idDevice = req.params.idDevice || req.body.idDevice;
  var status = req.params.status || req.body.status;
  // console.log('api dev ping: '+ idDevice);
  res.setHeader('Content-Type', 'application/json');
  console.log(idDevice);
  Device.find({_id:  idDevice}).exec(function(err, devices){
    // console.log(idDevice);

    console.log("ATUALIZANDO LUZ NO SERVIDOR PARA: "+status);

    if(devices.length>0){

      if(status == 1){

        devices[0].lightStatus = true;
      } else if(status == 0){
        devices[0].lightStatus = false
      }

      devices[0].save();

      var intLightStatus;

      if(devices[0].lightStatus){
        intLightStatus = 1;
      } else {
        intLightStatus = 0;
      }

      return res.send({
        temperature: devices[0].temperature,
        lightTimer: devices[0].lightTimer,
        lightDuration: devices[0].lightDuration,
        waterTimer: devices[0].waterTimer,
        lightStatus: intLightStatus
      });
    }

  });
};

// exports.pingTemp = function (req, res) {
//   var idDevice = req.params.idDevice || req  .body.idDevice;
//   res.setHeader('Content-Type', 'application/json');
//   Device.find({_id:  idDevice}).exec(function(err, devices){
//     // console.log(idDevice);
    
//     return res.send({temp: devices[0].temperature});
//   });
// };

// exports.pingWater = function (req, res) {
//   var idDevice = req.params.idDevice || req.body.idDevice;
//   res.setHeader('Content-Type', 'application/json');
//   Device.find({_id:  idDevice}).exec(function(err, devices){
//     // console.log(idDevice);
    
//     return res.send({water: devices[0].waterTimer});
//   });
// };

// exports.pingLight = function (req, res) {
//   var idDevice = req.params.idDevice || req.body.idDevice;
//   res.setHeader('Content-Type', 'application/json');
//   Device.find({_id:  idDevice}).exec(function(err, devices){
//     // console.log(idDevice);
    
//     return res.send({light:devices[0].lightTimer});
//   });
// };

exports.validate = function(req, res){
  if(req.user){
     return res.render('devices/validate');
  } else {
    return res.redirect('/login');
  }
};



exports.validateAction = function(req, res){
  if(req.user){
    Device.findOne({ _id: req.body.key }).exec(function(err, device){

      if(device.user){
        Device.find({ user: req.user }).exec(function(err, devices){
          return res.render('devices/all', {devices: devices, message: 'ERRO: E-stufa já foi cadastrada!', user: req.user, role: req.user.role});
        });
      }

      else{
        device.user = req.user;
        device.save();
        Device.find({ user: req.user }).exec(function(err, devices){
          return res.render('devices/all', {devices: devices, message: 'E-stufa adicionada com sucesso!', user: req.user, role: req.user.role});
        });
      }
    });

  } else {
    return res.redirect('/login');
  }
};

exports.restValidateAction = function(req, res){

  var userID = req.params.idUser;
  var deviceID = req.params.idDevice;
  var message = '';

  User.findOne({_id: userID}).exec(function(err, user){
    if(user){
      Device.findOne({_id: deviceID}).exec(function(err, device){
        if(device && !device.user){
          device.user = user;
          device.save();
          return res.send({success: true, message: 'Dispositivo cadastrado com sucesso!'});
        } else {

          if(device.user){
            message ='Dispositivo já cadastrado';
          } else {
            message = 'Dispositivo não encontrado';
          }
          return res.send({success: false, message: message});
        }
      });
    } else {
      return res.send({success: false, message: 'Usuário não encontrado'});
    }
  });
};

exports.new = function(req, res){
  if(req.user){
      Culture.find().exec(function(err, cultures){

        return res.render('devices/new', {cultures: cultures});
      });
    
  } else {
    return res.redirect('/login');
  }
};


