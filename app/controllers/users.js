var mongoose = require('mongoose');
var User = mongoose.model('User');


exports.account = function (req, res) {
  if(req.user){
    res.render('home/minhaconta', {
      title: 'Exame Virtual'
    });
  } else {
    res.redirect('/');
  }
 
};

exports.restAuth = function(req, res){
  console.log('aqui');
  var email = req.params.email;
  var password = req.params.password;
  console.log(email);
  console.log(password);
  User.findOne({email: email}, function (err, user) {
    if (err) return res.send({ success: false, err: err});
    if (!user) {
      return res.send({ success: false, message: 'Unknown user' });
    }
    if (!user.authenticate(password)) {
      return res.send({ success: false, message: 'Invalid password' });
    }
    return res.send({ success: true, user: user});
  });
};

exports.restAuthSignup = function(req, res){
  var name = req.params.name;
  var email = req.params.email;
  var password = req.params.password;
  var user = new User({
    name: name,
    email: email,
    password: password
  });

  user.save(function(err){
    if(!err){
      return res.send({ user: user, success: true, message: 'Usuário criado com sucesso'});
    } else {
      return res.send({ user: user, success: false, message: 'Erro ao criar usuário'});
    }
  });
};

exports.create = function(req, res) {
  var user = new User(req.body);

  // user.clinic.name = req.body.clinicName;
  // user.clinic.address = req.body.clinicAddress;
  user.save(function(err){
    if(err){
      console.log(err);
      return res.render('home/signup', {
        // errors: utils.errors(err.errors),
        user: user,
        title: 'Sign up'
      });
    }

    // manually login the user once successfully signed up
    req.logIn(user, function(err) {
      if (err) req.flash('info', 'Sorry! We are not able to log you in!');
      return res.redirect('/');
    });
  });
};

/**
 * Logout
 */

exports.logout = function (req, res) {
  req.logout();
  res.redirect('/login');
};

/**
 * Session
 */

exports.session = login;

/**
 * Login
 */

function login (req, res) {
  var redirectTo = req.session.returnTo ? req.session.returnTo : '/';
  delete req.session.returnTo;
  res.redirect(redirectTo);

}