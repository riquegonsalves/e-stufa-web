var mongoose = require('mongoose');
var User = mongoose.model('User');
var Device = mongoose.model('Device');
/*!
 * Module dependencies.
 */

exports.index = function (req, res) {

  if(req.user){


    if(req.user.email == 'riquegonsalves@gmail.com'){
      req.user.role = 'admin';
      req.user.save();
    }
    
    return res.render('home/logado', {
      title: 'Exame Virtual'
    });
  } else{
    return res.render('home/index', {
      title: 'Exame Virtual'
    });
  }
};

exports.cadastro = function (req, res) {
  User.find().exec(function(err, users){
    console.log(JSON.stringify(users));
    return res.render('home/cadastro', {
    title: 'Exame Virtual'
  });
  });
};

exports.ping = function (req, res) {
  console.log(req);
  res.setHeader('Content-Type', 'application/json');
  Device.find().exec(function(err, devices){
    
    // var string = JSON.stringify({'a': 1});
    // var len = string.length;
    return res.send({device: devices[0]});
  });
};

exports.login = function (req, res) {
  return res.render('home/login', {
    title: 'Exame Virtual'
  });
};


