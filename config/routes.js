
/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var home = require('home');
var devices = require('devices');
var cultures = require('cultures');
var pedidos = require('pedidos');
var users = require('users');

/**
 * Expose
 */

module.exports = function (app, passport) {

  app.get('/', home.index);
  // app.get('/logado', home.logado);  
  app.get('/login', home.login);  
  app.get('/cadastro', home.cadastro);  
  app.get('/devices/new', devices.new);  
  app.get('/devices/all', devices.all);
  app.get('/devices/validate', devices.validate);


  // CULTURES
  app.get('/culture/new', cultures.new);  
  app.get('/culture/all', cultures.all);
  app.get('/culture/:idCulture', cultures.viewCulture);
  app.get('/culture/edit/:idCulture', cultures.editView);
  app.post('/culture/edit', cultures.edit);
  app.post('/culture', cultures.createCulture);
  
  // app.post('/ping', home.ping);  
  // app.get('/pedidos/novo', pedidos.new);  
  app.get('/account', users.account);  

  app.post('/users', users.create);

  app.post('/device', devices.createDevice);

  // app.post('/pedidos/', pedidos.viewPedido);
  app.get('/device/:idDevice', devices.viewDevice);
  app.get('/device/edit/:idDevice', devices.editView);

  app.post('/device/edit', devices.edit);
  app.post('/device/validate', devices.validateAction);




  /**
   * Login
   */
  app.post('/session', passport.authenticate('local', {failureRedirect: '/login', failureFlash: 'Invalid email or password.'}), users.session);

  app.get('/logout', users.logout);

  /**
   * Error handling
   */

  app.use(function (err, req, res, next) {
    // treat as 404
    if (err.message
      && (~err.message.indexOf('not found')
      || (~err.message.indexOf('Cast to ObjectId failed')))) {
      return next();
    }
    console.error(err.stack);
    // error page
    res.status(500).render('500', { error: err.stack });
  });

  // assume 404 since no middleware responded
  app.use(function (req, res, next) {

    console.log(req.originalUrl);


    res.status(404).render('404', {
      url: req.originalUrl,
      error: 'Not found'
    });
  });
};
