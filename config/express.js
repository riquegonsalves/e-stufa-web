
/**
 * Module dependencies.
 */

var express = require('express');
var session = require('express-session');
var compression = require('compression');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var csrf = require('csurf');
var home = require('home');
var devices = require('devices');
var users = require('users');




var mongoStore = require('connect-mongo')(session);
var flash = require('connect-flash');
var winston = require('winston');
var helpers = require('view-helpers');
var config = require('config');
var pkg = require('../package.json');

var env = process.env.NODE_ENV || 'development';





/**
 * Expose
 */


module.exports = function (app, passport) {

  var api = createApiRouter();

  function createApiRouter() {
    var router = new express.Router()
    router.post('/ping', home.ping);
    router.post('/dev/:idDevice', devices.ping);
    // router.post('/dev/:idDevice', devices.ping);
    router.post('/dev/light/:idDevice/:status', devices.lightStatus);
    router.post('/dev/update/:idDevice/:temperature/:lightTimer/:lightDuration/:waterTimer/:lightStatus', devices.restUpdate);
    router.post('/auth/:email/:password', users.restAuth);
    router.post('/auth/signup/:email/:password/:name', users.restAuthSignup);
    router.post('/dev/all/:userid', devices.restDevices);
    router.post('/dev/validate/:idDevice/:idUser', devices.restValidateAction);
    // router.post('/dev/:idDevice/temp', devices.pingTemp);
    // router.post('/dev/:idDevice/water', devices.pingWater);
    // router.post('/dev/:idDevice/light', devices.pingLight);

    return router
  }

  app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'accept, content-type, x-parse-application-id, x-parse-rest-api-key, x-parse-session-token');
     // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
  });


  app.use('/api',api);
  // Compression middleware (should be placed before express.static)
  app.use(compression({
    threshold: 512
  }));

  // Static files middleware
  app.use(express.static(config.root + '/public'));

  // Use winston on production
  var log;
  if (env !== 'development') {
    log = {
      stream: {
        write: function (message, encoding) {
          winston.info(message);
        }
      }
    };
  } else {
    log = 'dev';
  }

  // Don't log during tests
  // Logging middleware
  if (env !== 'test') app.use(morgan(log));

  // set views path and default layout
  app.set('views', config.root + '/app/views');
  app.set('view engine', 'jade');

  // expose package.json to views
  app.use(function (req, res, next) {
    res.locals.pkg = pkg;
    res.locals.env = env;
    next();
  });

  // bodyParser should be above methodOverride
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  app.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
      // look in urlencoded POST bodies and delete it
      var method = req.body._method;
      delete req.body._method;
      return method;
    }
  }));

  // cookieParser should be above session
  app.use(cookieParser());
  app.use(cookieSession({ secret: 'secret' }));
  app.use(session({
    secret: pkg.name,
    proxy: true,
    resave: true,
    saveUninitialized: true,
    store: new mongoStore({
      url: config.db,
      collection : 'sessions'
    })
  }));
  require('dns').lookup(require('os').hostname(), function (err, add, fam) {
  console.log('addr: '+add);
})

  // use passport session
  app.use(passport.initialize());
  app.use(passport.session());

  // connect flash for flash messages - should be declared after sessions
  app.use(flash());

  // should be declared after session and flash
  app.use(helpers(pkg.name));
  // app.use(function(req, res, next){
  //   var whiteList = new Array("/ping");
  //   if (whiteList.indexOf(req.path) != -1) {
 
  //     boolean = true;
  //   } else {
  //     boolean = false;
  //   }
  //   next();
  // });


  // adds CSRF support
  app.use(csrf());

  app.use(function(req, res, next){   
    res.locals.csrf_token = req.csrfToken();  
    next();
  });

 

  // if (process.env.NODE_ENV !== 'test') {

  //   app.use(function(req, res, next){

  //     // var whiteList = new Array("/cadastro");
  //     // if (whiteList.indexOf(req.path) == -1) {
  //       csrf();  

  //       console.log(req);
  //       // console.log(csrf());
  //       // res.locals.csrf_token = req.csrfToken();
  //     // } 
  //     next();
  //   });
    

   
  // }
};


