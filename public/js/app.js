var $ = jQuery;
$(document).ready(function(){
	
	

	$('.btn-menu').click(function(){
		$('#menu').toggleClass('open');
	});

  $('#user').searchableOptionList({
      maxHeight: '250px'
  });

  $('form.formNew').on('keyup keypress', function(e) {
	  var code = e.keyCode || e.which;
	  if (code == 13) { 
	    e.preventDefault();
	    return false;
	  }
	});


	$('button.print').click(function(){
		$(this).hide();
		var page = document.body.innerHTML;
		$('#orders').wrap('<div id="orders" class="show"></div>');
		var print = $('#orders').html();
		$('body').css('padding-top', '10px');
		document.body.innerHTML = print;
		window.print();
		document.body.innerHTML = page;
		$('body').css('padding-top', '80px');

	
	});

	$(document).on("change","select",function(){
	  $("option[value=" + this.value + "]", this)
	  .attr("selected", true).siblings()
	  .removeAttr("selected");

	  if($('select#culture option[selected]').html() == 'Personalizado'){
			$('.advanced').show();
		} else {
			$('.advanced').hide();
		}
	});

	if($('select#culture option[selected]').length){
		if($('select#culture option[selected]').html() == 'Personalizado'){
			$('.advanced').show();
		} else {
			$('.advanced').hide();
		}
	} else {
		$('.advanced').show();
	}

	

});